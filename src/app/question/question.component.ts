import { Component, OnInit, Input } from '@angular/core';
import { QuestionService} from '../question.service';
import { MatStepper } from '@angular/material/stepper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  public _listQues;
  constructor( private ques: QuestionService, private router: Router) { }
  operation: any;
  ngOnInit(): void {
    this._listQues = this.ques.valReg();
    this.operation = localStorage.getItem("Operation");
    console.log(this.operation);
  }
  goForward(stepper: MatStepper, index: any){
    stepper.next();
    if(index == this._listQues.length -1){

      if(this.operation=="Specialist Outpatient Clinics"){
        this.router.navigate(['soc']);
      }
      else if(this.operation=="Inpatient Wards"){
        this.router.navigate(['inpatient']);
      }
      else {
        this.router.navigate(['general']);
      }
    }
}
}
