import { componentFactoryName } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralComponent } from './general/general.component';
import { InpatientComponent } from './inpatient/inpatient.component';
import { InvalidPatientComponent } from './invalid-patient/invalid-patient.component';
import { QuestionComponent } from './question/question.component';
import { RegSuccessComponent } from './reg-success/reg-success.component';
import { RegUnsuccessComponent } from './reg-unsuccess/reg-unsuccess.component';
import { SocComponent } from './soc/soc.component';
import { TitleComponent } from './title/title.component';
import { ValidPatientComponent } from './valid-patient/valid-patient.component';




const routes: Routes = [
  {path: '', redirectTo: 'title', pathMatch:'full'},
  {path: 'title', component:TitleComponent},
  {path: 'question', component:QuestionComponent},
  {path: 'soc', component:SocComponent},
  {path: 'inpatient', component:InpatientComponent},
  {path: 'general', component:GeneralComponent},
  {path: 'patient_valid', component:ValidPatientComponent},
  {path: 'patient_invalid', component:InvalidPatientComponent},
  {path: 'success', component:RegSuccessComponent},
  {path: 'unsuccess', component:RegUnsuccessComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
