import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TemperatureService {
  public temperature:any[]=[
    {
      "temperature": "High"
    },
    {
      "temperature": "Normal"
    }
  ];
  public fever:any[]=[
    {
      "fever": "Yes"
    },
    {
      "fever": "No"
    }
  ];
  constructor() { }
  valtemp(){
    return this.temperature;
  }
  valfever(){
    return this.fever;
  }
  temperatureValidation(temperature: any, fever: any){
    if(temperature= 'Normal' && fever =='Yes'){
      return true;
    }
    else{
      return false;
    }
  }
}
