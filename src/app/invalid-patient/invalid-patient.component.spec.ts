import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidPatientComponent } from './invalid-patient.component';

describe('InvalidPatientComponent', () => {
  let component: InvalidPatientComponent;
  let fixture: ComponentFixture<InvalidPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvalidPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
