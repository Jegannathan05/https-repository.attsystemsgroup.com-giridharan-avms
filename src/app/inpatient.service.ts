import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class InpatientService {
  public wardlist:any[]=[
    {
      "ward": "Ward01"
    },
    {
      "ward": "Ward02"
    },
    {
      "ward": "Ward03"
    }
  ];
  public roomlist:any[]=[
    {
      "room": "Room01"
    },
    {
      "room": "Room02"
    },
    {
      "room": "Room03"
    }
  ];
  public bedlist:any[]=[
    {
      "bed": "Bed01"
    },
    {
      "bed": "Bed02"
    },
    {
      "bed": "Bed03"
    }
  ];
  constructor() { }
  valward(){
    return this.wardlist;
  }
  valroom(){
    return this.roomlist;
  }
  valbed(){
    return this.bedlist;
  }
  inpatientValidation(patientName: any, location: any){
    if(patientName.substr(0,1) == "A" && location =="Ward01"){
      return true;
    }
    else{
      return false;
    }
  }
}