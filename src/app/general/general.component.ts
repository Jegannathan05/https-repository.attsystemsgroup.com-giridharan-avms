import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../general.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {
  public LocList2:any;
  constructor(private LocList1: GeneralService, private router: Router) { }

  ngOnInit(): void {
    this.LocList2=this.LocList1.valLoc();
  }
  onSubmit(){
    this.router.navigate(['patient_valid'])
  }
}

