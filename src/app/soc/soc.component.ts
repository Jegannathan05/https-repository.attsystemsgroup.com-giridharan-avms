import { Component, OnInit } from '@angular/core';
import {SocService} from './../soc.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-soc',
  templateUrl: './soc.component.html',
  styleUrls: ['./soc.component.css']
})
export class SocComponent implements OnInit {
  public listsoc: any;
  patientName: any;
  Location:any;
  constructor(private soclist1: SocService, private router: Router) { }

  ngOnInit(): void {
    this.listsoc=this.soclist1.valsoc();
  }
  getVal(item){
    console.log(item.target.value);
    this.patientName = item.target.value;
  }
  getSelectedDropdown(item) {
    console.log(item.target.innerHTML); 
    this.Location = item.target.innerHTML;   
  }
  onSubmit(){
    let validationResult = this.soclist1.inpatientValidation(this.patientName, this.Location);
    console.log(validationResult);
    if(validationResult==true){
      this.router.navigate(['patient_valid']);
    }
    else{
      this.router.navigate(['patient_invalid']);
    }
  }
}
