import { Component, OnInit } from '@angular/core';
import {InpatientService} from '../inpatient.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inpatient',
  templateUrl: './inpatient.component.html',
  styleUrls: ['./inpatient.component.css']
})
export class InpatientComponent implements OnInit {
  public wardlist2:any;
  public roomlist2:any;
  public bedlist2:any;
  patientName: any;
  Location: any;
  constructor(private wardlist1:InpatientService, private roomlist1: InpatientService, private bedlist1: InpatientService, private router: Router ) { }

  ngOnInit(): void {
    this.wardlist2=this.wardlist1.valward();
    this.roomlist2=this.roomlist1.valroom();
    this.bedlist2=this.bedlist1.valbed();
    
  }
  getVal(item){
    console.log(item.target.value);
    this.patientName = item.target.value;
  }
  getSelectedDropdown(item) {
    console.log(item.target.innerHTML); 
    this.Location = item.target.innerHTML;   
  }
  onSubmit(){
    let validationResult = this.wardlist1.inpatientValidation(this.patientName, this.Location);
    console.log(validationResult);
    if(validationResult==true){
      this.router.navigate(['patient_valid']);
    }
    else{
      this.router.navigate(['patient_invalid']);
    }
  }

}
