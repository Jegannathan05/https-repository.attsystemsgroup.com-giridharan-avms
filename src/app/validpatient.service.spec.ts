import { TestBed } from '@angular/core/testing';

import { ValidpatientService } from './validpatient.service';

describe('ValidpatientService', () => {
  let service: ValidpatientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidpatientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
