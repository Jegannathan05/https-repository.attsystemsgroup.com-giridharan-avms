import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  public LocList: any[] = [
    {
    "Loc": "Singapore"
    },
    {
    "Loc": "Hougang"
    },
    {
      "Loc": "Tampines"
    },
    {
      "Loc": "Pasir Ris"
    }
    ];
  constructor() { }
  valLoc() {
    return this.LocList;
    }
}
