import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, Validators, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ValidpatientService } from '../validpatient.service';
import { BlacklistService } from '../blacklist.service';

@Component({
  selector: 'app-valid-patient',
  templateUrl: './valid-patient.component.html',
  styleUrls: ['./valid-patient.component.css']
})
export class ValidPatientComponent implements OnInit {
  profileForm: any;
  validationResult: any;
  blacklist1:any;
  constructor(private _validator: ValidpatientService, private router: Router, private blist:BlacklistService) { }
  ngOnInit(): void {
    this.profileForm = new FormGroup({
      patNRIC: new FormControl(''),
    }, { validators: this.NRICValidator })
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.log(this.profileForm.value);
  }
  getVal(item){
    this.blacklist1=item.target.value;
  }

  NRICValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const patNRIC = control.get('patNRIC');
    let validation = this.checkValidation(patNRIC.value)
    return patNRIC && validation ? { identityRevealed: true } : null;
  };

  checkValidation(firstName: String) : boolean{
    console.log("here");
    let prefix = firstName.substr(0,1);
    console.log(prefix);
    if(prefix == "S" || prefix == "T" || prefix == "F" || prefix == "G"){
      let validation : boolean;
      if(prefix == "S" || prefix == "T")
        validation = this._validator.check_nric(firstName);
      if(prefix == "F" || prefix == "G")
        validation = this._validator.check_fin(firstName);
      if(validation)
        this.validationResult = "Valid NRIC";
      else
        this.validationResult = "InValid NRIC";
      return true;
    }
    
  }
  onclick()
{
  let validationResult = this.blist.blacklistValidation(this.blacklist1);
  console.log(validationResult);
  if(validationResult==true){
    this.router.navigate(['success']);
  }
  else{
    this.router.navigate(['unsuccess']);
  }
}

}

