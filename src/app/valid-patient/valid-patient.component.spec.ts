import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidPatientComponent } from './valid-patient.component';

describe('ValidPatientComponent', () => {
  let component: ValidPatientComponent;
  let fixture: ComponentFixture<ValidPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
